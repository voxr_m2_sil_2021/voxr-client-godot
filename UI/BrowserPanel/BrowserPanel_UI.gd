extends Control
# This handles everything the Browser Panel does
class_name BrowserPanel_UI

# Signals that a DataFrame has been selected, sends its name
signal dataframe_selected(name)
# Signals that a DataFrame's Stats have been selected, send its name
signal dataframe_stats_selected(name)

# References to meaningful nodes

# The DataFrame list
onready var list : Tree = $BrowserContainer/SelectionContainer/DataFrameList
# The field for the URL
onready var from_text : LineEdit = $BrowserContainer/LoadContainer/FormContainer/LoadFieldsContainer/LoadFromContainer/LoadFromLineEdit
# The field for the defined DataFrame's name
onready var to_text : LineEdit = $BrowserContainer/LoadContainer/FormContainer/LoadFieldsContainer/LoadToContainer/LoadToLineEdit
# The URL history as displayed
onready var history_disp : Tree = $BrowserContainer/LoadContainer/History
# The Label giving information on the logged in user
onready var me_label : Label = $BrowserContainer/SelectionContainer/MeLabel
# The timer to automatically refresh the DataFrame list 
onready var timer : Timer = $Timer
# The content of the URL history
onready var history : Array


# Path to the saved URL history
const save_path = "user://voxr_browser_history.dat"

# Called when the node enters the scene tree for the first time.
func _ready():
	# Ask for logged user info
	voxRequest.me(self, "_on_me_received")
	# Fetch the list of DataFrames
	update_list()
	# Load the URL history from disk
	load_history()

# Helper function to get the selected DataFrame's name, or null if none selected
func get_selected():
	var selected = list.get_selected()
	if(selected == null):
		return null
	return selected.get_text(0)

# Retrieve the DataFrame list, and restart the timer to postpone the next
# automatic update
func update_list():
	voxRequest.get_names(self, "_on_names_recieved")
	timer.start()

# Update the displayed history
func update_history():
	# Save it accordingly (no clean quitting process on Oculus yet, can't be
	# done neatly on application close.
	save_history()
	history_disp.clear()
	history_disp.set_columns(1)
	history_disp.create_item()
	for add in history:
		history_disp.create_item().set_text(0, add)

# Load the URL history from disk
func load_history():
	var file = File.new()
	file.open(save_path, File.READ)
	var read_history = file.get_var()
	
	# Don't set the history if the file contains none
	if typeof(read_history) != TYPE_NIL :
		history = read_history
	else :
		history = []
	file.close()
	# Update the displayed history accordingly
	update_history()

# Save the URL history to file
func save_history():
	var file = File.new()
	file.open(save_path, File.WRITE)
	file.store_var(history)
	file.close()

# Called when the DataFrame names are received
func _on_names_recieved(result, response_code, _headers, data):
	# Remember the selected one
	# To reselect it if still present, otherwise every automatic update
	# deselects in an annoying way
	var selected = get_selected()
	
	# Clear the current displayed list
	list.clear()
	
	# If the server responded OK
	if(result == 0 and response_code == 200):
		# Fill the list with received names
		list.set_columns(1)
		list.create_item().set_expand_right(0, false)
		for name in data :
			list.create_item().set_text(0, name)
	
	# If no DataFrame was selected, we are done
	if(selected == null): return
	# Otherwise, we look for it and reselect it if still there
	var item = list.get_root().get_children()
	while not item == null:
		if(item.get_text(0) == selected):
			item.select(0)
			return
		item = item.get_next()

# Called when the user clicks Delete
func _on_delete_pressed():
	# If no DataFrame is selected, there is nothing to do
	var selected = get_selected()
	if(selected == null): return
	# Otherwise, we ask the server for a delete and for a DataFrame list update
	voxRequest.delete_data(selected,
						   self)
	update_list()

# Called when the user presses Get
func _on_get_pressed():
	# If no DataFrame is selected, there is nothing to do
	var selected = get_selected()
	if(selected == null):
		return
	# Otherwise, signal that the user asked for data
	emit_signal("dataframe_selected", selected)

#Called when the user presses Get Stats
func _on_getStats_pressed():
	# If no DataFrame is selected, there is nothing to do
	var selected = get_selected()
	if(selected == null):
		return
	# Otherwise, signal that the user asked for stats
	emit_signal("dataframe_stats_selected", selected)

# Called when the user presses Load
func _on_load_pressed():
	# Ask the server to load the csv at the given URL into the given name
	voxRequest.load_data(to_text.text,
						 from_text.text,
						 self)
	# Ask for an updated list
	update_list()

	# Push the URL at the top of history, delete the old occurence if present
	history.erase(from_text.text)
	history.push_front(from_text.text)
	# Update the displayed history
	update_history()

# Called when the user select an entry in the URL history
func _on_history_cell_selected():
	# Set the URL field to the given URL
	from_text.text = history_disp.get_selected().get_text(0)
	# Set the DataFrame name to the file's basename, to avoid typing it in most cases
	to_text.text = from_text.text.get_file().get_basename()
	pass # Replace with function body.

# Called when information on the logged in user is received
func _on_me_received(result, response_code, _headers, data):
	# If the serer responded OK
	if(response_code == 200):
		# Display information
		me_label.text = "Logged in as %s %s (%s)" % [data["firstName"], data["lastName"], data["email"]]

# Called when this node disappear
func _exit_tree():
	# Save this history
	# Sadly, this is not called, nor any other cose mechanism, on Oculus
	save_history()

func logout():
	voxRequest.clear_token()
	get_tree().change_scene("res://Scenes/Login.tscn")

func _on_logout_pressed():
	logout()
