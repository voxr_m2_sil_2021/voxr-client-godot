extends MeshInstance


# Called when the node enters the scene tree for the first time.
func _ready():
	var data : PoolVector3Array = []
	var rng = RandomNumberGenerator.new()
	
	for i in range(10000):
		data.append(Vector3(rng.randf()-0.5,
							rng.randf()-0.5,
							rng.randf()-0.5))
	set_verts(data)
	print("cloud done !")

func set_verts(data : PoolVector3Array):
	var arr = []
	arr.resize(Mesh.ARRAY_MAX)
	
	arr[Mesh.ARRAY_VERTEX] = data

	var arrayMesh = ArrayMesh.new()
	#Actually make it a mesh
	arrayMesh.add_surface_from_arrays(Mesh.PRIMITIVE_POINTS, arr)
	self.mesh = arrayMesh
