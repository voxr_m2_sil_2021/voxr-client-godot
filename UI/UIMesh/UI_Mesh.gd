extends VREventHandler
class_name UI_Mesh

var mesh
var meshtool
onready var camera = get_node("../OQ_ARVROrigin/OQ_ARVRCamera")
export var resolution : Vector2 = Vector2(640, 360)
export var UI_path : NodePath = "UI"

func _ready():
	mesh = $MeshInstance.get_mesh()
	meshtool = MeshDataTool.new()
	var error = meshtool.create_from_surface(mesh, 0)
	print("MeshTool.create_from_surface returns : " + str(error))
	viewport = $Viewport
	if viewport != null : print("Viewport not found on " + str(self))
	
	#Automatically set up the correct collisionShape
	if $CollisionShape != null :
		remove_child($CollisionShape)
	var shape = mesh.create_trimesh_shape()
	var shapeNode = CollisionShape.new()
	shapeNode.shape = shape
	add_child(shapeNode)
	
	#Automatically instanciate the correct viewport with given scene
	viewport = Viewport.new()
	viewport.size = resolution
	viewport.render_target_v_flip = true
	
	var colorRect = ColorRect.new()
	colorRect.anchor_bottom = 1
	colorRect.anchor_right = 1
	viewport.add_child(colorRect)
	
	reparent_control(get_node(UI_path), viewport)
	
	var material = SpatialMaterial.new()
	material.flags_unshaded = true
	material.albedo_texture = viewport.get_texture()
	
	$MeshInstance.material_override = material
	add_child(viewport)

func get_face(point):
	for idx in range(meshtool.get_face_count()):
		# Normal is the same-ish, so we need to check if the point is on this face
		var v1 : Vector3 = meshtool.get_vertex(meshtool.get_face_vertex(idx, 0)) 
		var v2 : Vector3 = meshtool.get_vertex(meshtool.get_face_vertex(idx, 1))
		var v3 : Vector3 = meshtool.get_vertex(meshtool.get_face_vertex(idx, 2))
		
		if is_point_in_triangle(point, v1, v2, v3):
			return idx
	return null

func barycentric(p : Vector3, a : Vector3, b : Vector3, c: Vector3) -> Vector3 :
	var v0 := b - a
	var v1 := c - a
	var v2 := p - a
	var d00 := v0.dot(v0)
	var d01 := v0.dot(v1)
	var d11 := v1.dot(v1)
	var d20 := v2.dot(v0)
	var d21 := v2.dot(v1)
	var denom := d00 * d11 - d01 * d01
	var v = (d11 * d20 - d01 * d21) / denom
	var w = (d00 * d21 - d01 * d20) / denom
	var u = 1.0 - v - w
	return Vector3(u, v, w)

func is_point_in_triangle(point, v1, v2, v3):
	var bc = barycentric(point, v1, v2, v3)
	if bc.x < 0 or bc.x > 1:
		return false
	if bc.y < 0 or bc.y > 1:
		return false
	if bc.z < 0 or bc.z > 1:
		return false
	return true

# Gets the uv coordinates on the mesh given a point on the mesh and normal
# these values can be obtained from a raycast
func get_uv_coords(point):
	var face = get_face(point)
	if face == null:
		return null
	var v1 = meshtool.get_vertex(meshtool.get_face_vertex(face, 0))
	var v2 = meshtool.get_vertex(meshtool.get_face_vertex(face, 1))
	var v3 = meshtool.get_vertex(meshtool.get_face_vertex(face, 2))
	var bc = barycentric(point, v1, v2, v3)
	var uv1 = meshtool.get_vertex_uv(meshtool.get_face_vertex(face, 0))
	var uv2 = meshtool.get_vertex_uv(meshtool.get_face_vertex(face, 1))
	var uv3 = meshtool.get_vertex_uv(meshtool.get_face_vertex(face, 2))
	return (uv1 * bc.x) + (uv2 * bc.y) + (uv3 * bc.z)


func ui_raycast_hit_event(position, click, release):
	# note: this transform assumes that the unscaled area is [-0.5, -0.5] to [0.5, 0.5] in size
	var uv_coords = get_uv_coords(to_local(position))
	if(uv_coords == null) :
		return
	var pos2d = viewport.size * uv_coords

	if (click || release):
		var e = InputEventMouseButton.new();
		e.pressed = click;
		e.button_index = BUTTON_LEFT;
		e.position = pos2d;
		e.global_position = pos2d;

		viewport.input(e);
		
	elif (last_pos2d != null && last_pos2d != pos2d):
		var e = InputEventMouseMotion.new();
		e.relative = pos2d - last_pos2d;
		e.speed = (pos2d - last_pos2d) / 16.0; #?? chose an arbitrary scale here for now
		e.global_position = pos2d;
		e.position = pos2d;
		
		viewport.input(e);
	last_pos2d = pos2d;
	
	
func reparent_control(node,newParent):
	var oldParent = node.get_parent()
	oldParent.remove_child(node)
	newParent.add_child(node)

func _input(event):
	if event is InputEventKey:
		viewport.input(event)
