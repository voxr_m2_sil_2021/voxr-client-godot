tool
# This is a tool : it works in the editor, allowing to see the surface in the 3D
# editor view
extends MeshInstance

# Define a MeshInstance displaying a half cylinder of arbitrarily fine approximation
class_name BentSurface

# The number of facets used to approximate a half-cylinder
# Exported to editor, to enable using this without messing with the code
export var Facets = 8 setget set_N

# Called when the node enters the scene tree for the first time.
func _ready():
	# Generate a cylinder as soon as it is in the scene
	generate(Facets)

# Set a new number of facets
func set_N(var value):
	Facets = value
	# Need to regenerate the mesh
	generate(Facets)

# Generate a mesh with N facets
func generate(N = 8):
	# Declare the array containing the pools
	var arr = []
	arr.resize(Mesh.ARRAY_MAX)
	
	# Declare the actual data arrays
	# The vertices
	var verts = PoolVector3Array()
	verts.resize(2*(N+1))
	# The UVs
	var uvs = PoolVector2Array()
	uvs.resize(2*(N+1))
	# The indices
	var indices = PoolIntArray()
	indices.resize(6*(N))
	
	#Generate the Bent surface (a partial cylinder)
	for i in range(0, N+1):
			# Use cosinus and sinus to draw a partial circle.
			# Constant 1 and -1 on y axis for, well, a cylinder
			verts.set(2*i,Vector3(cos(float(i)/float(N)*PI), 1, sin(float(i)/float(N)*PI)))
			verts.set(2*i+1, Vector3(cos(float(i)/float(N)*PI),-1, sin(float(i)/float(N)*PI)))
	for i in range(0, N+1):
			# UVs are linear, to fit with the GUI naturally
			uvs.set(2*i, Vector2(float(i)/float(N),0))
			uvs.set(2*i+1, Vector2(float(i)/float(N),1))
			
	# Set the indices to "weave" the cylinder with the defined vertices
	for i in range(0, N):
			indices.set(6*i, 2*i)
			indices.set(6*i+1, 2*i+3)
			indices.set(6*i+2, 2*i+1)
			indices.set(6*i+3, 2*i)
			indices.set(6*i+4, 2*i+2)
			indices.set(6*i+5, 2*i+3)
			
	# Assign the pools in the right spots for an ArrayMesh
	arr[Mesh.ARRAY_VERTEX] = verts
	arr[Mesh.ARRAY_TEX_UV] = uvs
	arr[Mesh.ARRAY_INDEX] = indices

	# Declare an ArrayMesh, which inherits Mesh but allows creation from arrays
	var arrayMesh = ArrayMesh.new()
	# Actually make it a mesh
	arrayMesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arr)
	# Use it on display
	self.mesh = arrayMesh
