extends Panel


onready var login_username : LineEdit = $"TabContainer/Log in/LoginContainer/LoginUsernameContainer/LoginUsernameEdit"
onready var login_password : LineEdit = $"TabContainer/Log in/LoginContainer/LoginPasswordContainer/LoginPasswordEdit"
onready var login_remember : CheckBox = $"TabContainer/Log in/LoginContainer/LoginRememberContainer/LoginRememberCheckBox"

onready var signup_firstname : LineEdit = $"TabContainer/Sign up/SignupContainer/SignupFirstnameContainer/SignupFirstnameEdit"
onready var signup_lastname :  LineEdit = $"TabContainer/Sign up/SignupContainer/SignupLastnameContainer/SignupLastnameEdit"
onready var signup_username :  LineEdit = $"TabContainer/Sign up/SignupContainer/SignupUsernameContainer/SignupUsernameEdit"
onready var signup_password :  LineEdit = $"TabContainer/Sign up/SignupContainer/SignupPasswordContainer/SignupPasswordEdit"

onready var server_label : Label = $"TabContainer/Select Server/ServerContainer/ServerLabel"
onready var new_server : LineEdit = $"TabContainer/Select Server/ServerContainer/NewServerContainer/NewServerLineEdit"
var token : String


const url_save_path = "user://voxr_url.dat"

const save_path = "user://voxr_credentials.dat"
# Called when the node enters the scene tree for the first time.
func _ready():
	load_credentials()
	load_url()
	update_server_label()

func update_server_label():
	server_label.text = "Current server : " + voxRequest.URL

func load_credentials():
	var file = File.new()
	file.open(save_path, File.READ)
	var credentials = file.get_var()
	if typeof(credentials) != TYPE_NIL :
		login_username.text = credentials[0]
		if(credentials.size() > 1):
			login_password.text = credentials[1]
	file.close()

func save_credentials():
	var file = File.new()
	var credentials = [login_username.text]
	if(login_remember.pressed):
		credentials.append(login_password.text)
	file.open(save_path, File.WRITE)
	file.store_var(credentials)
	file.close()

func load_url():
	var file = File.new()
	file.open(url_save_path, File.READ)
	var url = file.get_var()
	if typeof(url) != TYPE_NIL :
		voxRequest.URL = url
		update_server_label()
	file.close()

func save_url():
	var file = File.new()
	var url = voxRequest.URL
	file.open(url_save_path, File.WRITE)
	file.store_var(url)
	file.close()

func _on_login_pressed():
	voxRequest.log_in(login_username.text,
					  login_password.text,
					  self,
					  "_on_token_received")

func _on_token_received(result, response_code, _headers, data):
	if(response_code == 200):
		voxRequest.set_token(data.get("token"))
		save_credentials()
		get_tree().change_scene("res://Scenes/WorkPlace.tscn")
	else:
		var error = $"TabContainer/Log in/LoginContainer/ErrorLabel"
		if error == null: 
			error = Label.new()
			error.name = "ErrorLabel"
			$"TabContainer/Log in/LoginContainer".add_child(error)
		error.text = "Login failed !"


func _on_signup_pressed():
	if(signup_firstname == null):
		print("first")
	if(signup_lastname == null):
		print("last")
	if(signup_username == null):
		print("user")
	if(signup_password == null):
		print("pass")
	voxRequest.sign_up(signup_firstname.text,
					   signup_lastname.text,
					   signup_username.text,
					   signup_password.text,
					   self,
					   "_on_signup_response")

func _on_signup_response(result, response_code, _headers, data):
	var error = $"TabContainer/Sign Up/SignupContainer/ErrorLabel"
	if error == null: 
		error = Label.new()
		error.name = "ErrorLabel"
		$"TabContainer/Sign up/SignupContainer".add_child(error)
	if(response_code == 200):
		error.text = "Succesfully signed up !"
	else:
		error.text = data["error"]



func _on_setServer_pressed():
	voxRequest.URL = new_server.text
	save_url()
	update_server_label()
	 
	pass # Replace with function body.
