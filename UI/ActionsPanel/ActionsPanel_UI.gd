extends Panel

# This scripts handles everything happening on the Actions Panel
class_name ActionsPanel_UI

# References to meaningful nodes

# The label indicating the selected DataFrame, or the temporary state if needed
onready var current_dataframe_label : Label = $VBoxContainer/CurrentDataFrameLabel
# The label indicating the currently selected columns
onready var selection_label : Label = $VBoxContainer/SlectionLabel
# The text field for the name of the DataFrame defined by Crop To
onready var crop_to_text_edit : LineEdit = $VBoxContainer/CropToContainer/CropToTextEdit
# The Crop To button
onready var crop_to_button : Button = $VBoxContainer/ColumnsButtonsContainer/CropToButton
# The MDS button
onready var mds_button : Button = $VBoxContainer/ColumnsButtonsContainer/MDSButton
# The Delete button
onready var delete_button : Button = $VBoxContainer/ColumnsButtonsContainer/DeleteButton
# The Filter condition text field
onready var filter_condition : LineEdit = $VBoxContainer/FilterConditionContainer/FilterConditionLineEdit
# The Filter name text field
onready var filter_name : LineEdit = $VBoxContainer/FilterNameContainer/FilterNameLineEdit
# The Filter button
onready var filter_button : Button = $VBoxContainer/FilterButtonsContainer/FilterButton

#The currently selected DataFrame's name
var current_dataframe : String

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# This "process" simply updates the label for the selected columns
	# TODO : do this via signals, to avoid redoing it every frame. Not urgent as
	# it does not consume much CPU
	
	# Header
	var string : String = "Selected columns : ["
	#Add every entry followed by a coma, except the last
	for i in range(context.selected_columns.size() - 1):
		string += context.selected_columns[i] + ", "
	#Add the last one, if there is one
	if(context.selected_columns.size() > 0):
		string += context.selected_columns.back()
	string += "]"
	#Actually update the label
	selection_label.text = string

# Called when Crop To is pressed
func _on_crop_to_pressed():
	# Ask the server for a Crop To on the current DataFrame
	voxRequest.crop_to(current_dataframe,
						context.selected_columns,
						crop_to_text_edit.text,
						self)

func _on_filter_pressed():# Ask the server for a Crop To on the current DataFrame
	voxRequest.filter(current_dataframe,
						filter_condition.text,
						filter_name.text,
						self)

# Called when data from a DataFrame is received
func _on_data_received(name):
	# Register the current DataFrame
	current_dataframe = name
	# Display it
	current_dataframe_label.text = "Current DataFrame : " + current_dataframe
	# Set a default name for an eventual Crop To (frequently avoid typing)
	crop_to_text_edit.text = current_dataframe + "_crop"
	# Enable the Crop To button
	crop_to_button.disabled = false

# Called when data without remote DataFrame is received
func _on_view_received():
	# Erase the current DataFrame's name - better safe than sorry
	current_dataframe = ""
	# Warn the user about the temporary view state
	current_dataframe_label.text = "Seeing temporary view !"
	# No need for a default Crop To name
	crop_to_text_edit.text = ""
	# Because Crop To is disabled
	crop_to_button.disabled = true


