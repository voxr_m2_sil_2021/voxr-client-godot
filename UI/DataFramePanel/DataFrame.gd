extends Tree
# We use a Tree for this. Seems weird but it is the best way with current
# builtin Godot node

# This implements a UI DataFrame
class_name DataFrame

# Store the selected columns' indices
# Their names are stored in context (cf Context.gd)
var selected_indices = []

# Given an Array of Arrays, fill the table with its data
# (GDScript doesn't yet allow to statically type the elements of an Array)
func fill_table(data:Array):
	# Display the headers
	set_column_titles_visible(true)
	# First clear preceding data
	self.clear()
	# Set the number of columns
	set_columns(data[0].size())
	# A root is mandatory. The tree is set to not display it anyway
	var itemData = create_item()
	
	# For each column
	for i in range(data[0].size()):
		# Set the header to the first line's value
		set_column_title(i, data[0][i])
	
	# Fore each line
	for i in range(1, data.size()):
		# Create a Tree line
		itemData = create_item()
		# For each item in the line
		for j in range(data[i].size()):
			# Set the corresponding text in the Tree
			itemData.set_text(j, data[i][j])

# Clear selected columns
func clear_selection():
	context.selected_columns = []
	selected_indices = []

# Select a column
func select_column(selected):
	selected_indices.push_back(selected)
	context.selected_columns.push_back(get_column_title(selected))
	
	# Select all cells in the actual Tree, to have a clearly displayed selection
	if get_root() != null:
		var child = get_root().get_children()
		while child != null:
			child.select(selected)
			child = child.get_next()

# Desekect a column
func deselect_column(selected):
	selected_indices.erase(selected)
	context.selected_columns.erase(get_column_title(selected))
	
	# Deselect the actual Tree items
	if get_root() != null:
		var child = get_root().get_children()
		while child != null:
			child.deselect(selected)
			child = child.get_next()

# Called when the user clicks on a column's name
func _on_column_title_pressed(selected):
	# Select or deselect the column
	if selected_indices.has(selected) :
		deselect_column(selected)
	else :
		select_column(selected)

# Called when the user clicks on a single cell
func _on_cell_selected():
	# Clear all selected columns
	clear_selection()
