extends Control
# This handles everything the DataFrame Panel does
class_name DataFramePanel_UI

# Signals that data have been received with its name
signal data_received(name)
# Signals that a temporary view has been received
signal view_received()

# Simple reference to the actual DataFrame UI
onready var df = $DataFrame

# variable for the current DataFrame name
var current_dataframe : String 

# Called when a DataFrame has been selected
func _on_dataframe_selected(name):
	# Ask the server for its data
	voxRequest.get_data(name, self, "_on_data_received")
	# Set the selected DataFrame
	current_dataframe = name

# Called when stats from a DataFrame have been selected
func _on_dataframe_stats_selected(name):
	# Ask the server for those stats
	voxRequest.get_stats(name, self, "_on_stats_received")
	# Erase the selected DataFrame's name
	current_dataframe = ""

# Called when data have been received
func _on_data_received(result, response_code, _headers, data : Array):
	# Fill the actual table with the data if the server sent OK
	if(result == 0 and response_code == 200):
		df.fill_table(data)
	# Signals that data has been received
	emit_signal("data_received", current_dataframe)

# Called when stats have been received
func _on_stats_received(result, response_code, _headers, data : Array):
	# Fill the table with the data if the server sent OK
	if(result == 0 and response_code == 200):
		df.fill_table(data)
	# Signal that stats have been received
	emit_signal("view_received")
