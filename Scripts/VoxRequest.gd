extends HTTPRequest
class_name VoxRequest

var URL : String

var queue : Array = []

var token : String

var current_headers : Array = ["Content-Type: application/json; charset=utf-8"]

signal response_parsed

class RequestData:
	var request : String
	var request_method : int
	var target : Object
	var target_method : String
	var binds : Array = []
	var body : String = "{}"

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("request_completed", self, "_on_request_completed")

func set_token(token: String):
	token = token
	current_headers = ["Content-Type: application/json; charset=utf-8",
					   "Authorization: Bearer %s" % token]

func clear_token():
	token = ""
	current_headers = ["Content-Type: application/json; charset=utf-8"]
### INTERNALS ###

func connect_request(request_data:RequestData):
	print("actually sending request to " + request_data.request)
	print("with headers : " + str(current_headers))
	var error = request(request_data.request,
						current_headers,
						true,
						request_data.request_method,
						request_data.body)
	handle_request_errors(request_data, error)
	if error != OK:
		queue.pop_front()
	return error

func queue_request(request_data:RequestData):
	queue.push_back(request_data)
	if(queue.size() == 1):
		connect_request(request_data)

func _on_request_completed(result, response_code, headers, body : PoolByteArray):
	var request_data = queue.pop_front()
	
	var error = 0
	if(not queue.empty()):
		connect_request(queue.front())
		
	var data = JSON.parse(body.get_string_from_utf8()).result
	
	print("on_request_completed :")
	print("Request result : " + str(result))
	print("Reponse code : " + str(response_code))
	print("Data : " + str(data))
	
	handle_request_results(request_data, result)
	
	if(response_code == 501):
		print("R error : " + data["error"])
		pop_error_on_node(request_data.target,
						  data["error"],
						  "R error")
	
	if(request_data.target != null and request_data.target_method != ""):
		request_data.target.call(request_data.target_method,
								 result,
								 response_code,
								 headers,
								 data)
								

func handle_request_errors(request_data:RequestData, error):
	if(error == ERR_INVALID_PARAMETER):
		print("Malformed URL !")
		pop_error_on_node(request_data.target,
						  "Seems like the current server is no valid URL : " + URL)
	
	elif(error == ERR_CANT_CONNECT):
		print("Can't connect to the server")
		pop_error_on_node(request_data.target,
						  "Can't connect to server : " + URL)

func handle_request_results(request_data:RequestData, result):
	if(result == HTTPRequest.RESULT_CANT_RESOLVE):
		print("Can't resolve URL !")
		pop_error_on_node(request_data.target,
						  "Can't resolve the current URL : " + URL)
	
	elif(result == HTTPRequest.RESULT_CANT_CONNECT or result == HTTPRequest.RESULT_CONNECTION_ERROR):
		print("Can't connect to the server")
		pop_error_on_node(request_data.target,
						  "Can't connect to server : " + URL)

func pop_error_on_node(node:Node, msg:String, title:String = ""):
	var error_popup : AcceptDialog = node.get_node("error_popup")
	var must_add : bool = false
	if error_popup == null:
		error_popup = AcceptDialog.new()
		error_popup.name = "error_popup"
		must_add = true
		
		
	error_popup.dialog_text = msg
	error_popup.window_title = "R error"
	
	if must_add:
		node.add_child(error_popup)
	var size = error_popup.get_rect().size
	
	error_popup.anchor_left = 0.5
	error_popup.anchor_right = 0.5
	error_popup.anchor_top = 0.5
	error_popup.anchor_bottom = 0.5

	error_popup.margin_left = -size.x / 2
	error_popup.margin_right = -size.x / 2
	error_popup.margin_top = -size.y / 2
	error_popup.margin_bottom = -size.y / 2
	
	error_popup.show()

### PUBLIC METHODS ###

func get_data(dataName:String, target:Object, method:String, binds:Array = []):
	var request_data:RequestData = RequestData.new()
	
	request_data.request = URL + "/R/data?dataName=" + dataName
	request_data.request_method = HTTPClient.METHOD_GET
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	
	queue_request(request_data)

func get_stats(dataName:String, target:Object, method:String, binds:Array = []):
	var request_data:RequestData = RequestData.new()
	
	request_data.request = URL + "/R/statsData?dataName=" + dataName
	request_data.request_method = HTTPClient.METHOD_GET
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	
	queue_request(request_data)

func get_names(target:Object, method:String, binds:Array = []):
	var request_data:RequestData = RequestData.new()
	
	request_data.request = URL + "/R/getDataNames"
	request_data.request_method = HTTPClient.METHOD_GET
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	
	queue_request(request_data)

func delete_data(dataName:String, target:Object = null, method:String = "", binds:Array = []):
	var request_data:RequestData = RequestData.new()
	
	request_data.request = URL + "/R/deletData?dataName=" + dataName
	request_data.request_method = HTTPClient.METHOD_DELETE
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	
	queue_request(request_data)

func crop_to(fromDataName:String, columnSelected:Array, toDataName:String, target:Object, method:String = "", binds:Array = []):
	if(columnSelected.empty()): return
	var request_data:RequestData = RequestData.new()
	var columnSelectedString : String = columnSelected[0]
	for i in range(1, columnSelected.size()):
		columnSelectedString += "//" + columnSelected[i]
		
	request_data.request = URL + "/R/cropFrom?dataName=" + fromDataName + "&" + "columnSelect=" + columnSelectedString + "&" + "newDataName=" + toDataName
	request_data.request_method = HTTPClient.METHOD_POST
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	
	queue_request(request_data)

func load_data(dataName:String, newCsvURL:String, target:Object = null, method:String ="", separator:String = ",", binds:Array = []):
	var request_data:RequestData = RequestData.new()
	
	request_data.request = URL + "/R/setData?path=" + newCsvURL + "&" + "dataName=" + dataName + "&" + "sep=" + separator
	request_data.request_method = HTTPClient.METHOD_POST
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	
	queue_request(request_data)

func log_in(username:String, password:String, target:Object, method:String, binds:Array = []):
	var request_data:RequestData = RequestData.new()
	
	request_data.request = URL + "/account/login"
	request_data.request_method = HTTPClient.METHOD_POST
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	request_data.body = "{\"Username\":\"%s\", \"Password\":\"%s\"}"\
					  % [username, password]
	queue_request(request_data)

func sign_up(firstName:String, lastName:String, username:String, password:String, target:Object, method:String, binds:Array = []):
	var request_data:RequestData = RequestData.new()
	
	request_data.request = URL + "/account"
	request_data.request_method = HTTPClient.METHOD_POST
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	request_data.body = "{\"Username\":\"%s\", \"Password\":\"%s\", \"FirstName\":\"%s\", \"LastName\":\"%s\"}"\
					  % [username, password, firstName, lastName]
	queue_request(request_data)

func me(target:Object, method:String, binds:Array = []):
	var request_data:RequestData = RequestData.new()
	
	request_data.request = URL + "/account/me"
	request_data.request_method = HTTPClient.METHOD_GET
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	
	queue_request(request_data)

func filter(dataName:String, condition:String, newDataName:String, target:Object, method:String = "", binds:Array = []):
	var request_data:RequestData = RequestData.new()
	
	request_data.request = URL + "/R/filter?dataName=" + dataName + ",condition=" + condition + ",newDataName=" + newDataName
	request_data.request_method = HTTPClient.METHOD_POST
	request_data.target = target
	request_data.target_method = method
	request_data.binds = binds
	
	queue_request(request_data)


