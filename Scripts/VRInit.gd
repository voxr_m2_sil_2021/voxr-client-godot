extends Spatial
class_name VRInit
# This is a simple script to initialize VR systems
onready var locomotion_stick = $OQ_ARVROrigin/Locomotion_Stick;

func _ready():
	vr.initialize();
