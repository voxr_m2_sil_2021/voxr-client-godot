extends Spatial

export var show_text_input := true;

var _keyboard = null;

signal text_input_cancel;
signal text_input_enter;


func _on_cancel():
	emit_signal("text_input_cancel");

func _on_leter_pressed(ev):
	$OQ_UI2DCanvas_TextInput._input(ev);

func _ready():
	_keyboard = $OQ_UI2DCanvas_Keyboard.find_node("VirtualKeyboard", true, false);
	
	if (show_text_input):
		_keyboard.connect("leter_pressed", self, "_on_leter_pressed");
		_keyboard.connect("cancel_pressed", self, "_on_cancel");
		_keyboard.connect("enter_pressed", self, "_on_enter");
