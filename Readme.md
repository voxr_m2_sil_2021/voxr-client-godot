![VoxR logo](VoxR_logo.png)

# CIR
[Lien vers le dossier Crédit Import Recherche](https://docs.google.com/document/d/11nF8ZgNdioYLvW8zDMQms2t2RxloM55SuNz07VkH9SM/edit?usp=sharing)

# VoxR Client

VoxR Client is an Oculus Quest 2 application, to use with a [VoxR Server](https://git.unistra.fr/voxr/voxr-r-server), allowing visualization and basic manipulation of R DataFrames, in virtual reality.

## Installation


  To install our app on your Oculus Quest 2, there are three mendatory things to do and then, either you can install it with godot or you can install it with the apk.

### (1) USB-C cable

  Make sure that you have an USB-C cable and a USB-3 port on your external device to connect it and your oculus quest.

### (2) Register as a developer

  Please register as a developer on the Oculus account associated with your Quest [here](https://dashboard.oculus.com/organizations/create/).

### (3) Enable Developer Mode on your Quest

  Open the Oculus app on the phone that is linked to your your Oculus Quest, with your Quest turned on and:

  1. Tap ‘Settings’ in the bottom right

  2. Locate your Oculus Quest listed in the Settings tab, and make sure it reads ‘Connected’. If the app can’t connect to your Quest, you may need to top on the Quest in settings to try and manually make the app connect. If it still can’t connect, make sure your phone has Bluetooth and WiFi turned on as well, and your Quest and your phone are on the same Internet connection.

  3. Tap on the arrow button next to your device, to reveal more options

  4. Tap the ‘More Settings’ button

  5. Tap on ‘Developer Mode’

  6. Flick the switch to On instead of Off

  7. Fully reboot your Quest – hold down the power button on the side and select ‘Power Off’ or ‘Restart’.

  After rebooting, your Quest should be in Developer Mode.

### Via Godot

  To install our application on your Oculus Quest 2, you can clone our [repository](https://git.unistra.fr/voxr/VoxRGodot) then please download [Godot](https://godotengine.org/download) and finally follow this Godot [Tutorial](https://docs.godotengine.org/en/stable/tutorials/vr/developing_for_oculus_quest.html) but with little tweaks :

  - Go through the step of [Exporting for Android](https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_for_android.html#doc-exporting-for-android) but do not download or change the setup of debug.keystore because we already did it in our project.
  
  - Do not download the Quest plugin as it is already in our project.
  
  - Do not Create a new project, instead Import ours by opening our project.godot file.

  - No need to modify our project.

  - No need to redo the first three step.
  
  - Simply follow the fourth last paragraph of the tutorial with the "little Android button".


### Via APK


  To install our application on your Oculus Quest 2, you can download a binary [here](https://seafile.unistra.fr/d/2ce70d02aa73419ba0c1/files/?p=%2FVoxR_Prototype.apk&dl=1) and follow our 2 step tutorial.
#### 1) Install SideQuest

  Please install [SideQuest](https://sidequestvr.com/#/download) on your external device.

#### 2) Allow USB debugging between your external device and your Quest

  1. Open SideQuest on your external device.

  2. Turn on your Quest and set up a guardian.

  3. Plug your Quest into your external device with the USB-C cable.

  4. Inside your Quest, not on your computer, you should see a request to “Allow USB debugging.”

  5. If you don’t want to have to press OK on this screen every time you plug your Quest into your computer, make sure the ‘Always allow from this computer’ box is ticked.

  6. Press OK.

  You can now use SideQuest to SideLoad our app on your oculus Quest 2 by clicking the install apk button at the top right corner of SideQuest

## Usage

### Overall

  Any UI panel in this application is movable at your convenience, using the grab trigger. You can click on any UI element with both of your controllers, with their index triggers.

  To ease the typing, a pop-up keyboard is available on your left hand. It will pop above your controller when you press its **Y** button. It will follow your controller, but you can grab it as well, to fine-tune it to your comfort.

### Logging in

When launching the app, you will first see a login prompt. You can either log in or sign up to a given server. You have to log in to use the client.

A third tab enables you to switch servers. This way, you can easily connect to your own hosted server. Every server handles its own userbase and DataFrames independently.

![VoxR login](.image/login.jpg)

### Working

When logged in, you arrive in the working space. Three panels will be available, from left to right: the Browser panel, the DataFrame panel, and
the Action panel

#### Browser Panel

![VoxR login](.image/browser_panel.jpg)

The browser panel is twofold.

##### The DataFrames list

On the left, the DataFrame list allows you to see, on live, the DataFrames defined on the remote server. If, for example, another user defines or deletes a DataFrame while you are logged in, you will promptly see those changes reflected in this list.

Three buttons are here below the list: Delete, Get and Get Stats:

- **Delete** is rather self-explanatory: it simply deletes the DataFrame on the remote server. 
- **Get** will retrieve the DataFrame from the remote server and display it on the DataFrame panel
- **Get Stats** will ask the remote server for stats on the selected DataFrame, and display those in the DataFrame panel.

##### The Loading form

On the right, the DataFrame loader enables you to load a CSV file on the remote server directly from the web, via its URL. Just type your URL, name the DataFrame to be defined, and press Load! The DataFrame will appear on the list, ready for use.

Just below this simple form is a history: any time you load a CSV, its URL is recorded in the history, so you won't have to type its URL again  (no one likes to type on a virtual keyboard!) This history is stored on disk, so you won't lose it between working sessions either!

#### DataFrame Panel

![VoxR login](.image/dataframe_panel.jpg)

This panel, bent for better visualization, will show any data asked from the remote server. You can scroll, by dragging the table itself, or by using the scrollbar on the right.

You can select columns by clicking on the headers, this selection will make sense for Columns Actions on the Actions panel.

#### Actions Panel

![VoxR login](.image/action_panel.jpg)

This panel exposes actions to be done on the DataFrames.

On the top of it, it indicates the name of the displayed DataFrame, on which actions will be done. **Warning**: some actions, such as Get Stats, will return a temporary view to the client: those are data you can visualize, but not stored on the server. So currently, no action can be done on those, and the Action panel will nicely warn you about this state.

Below this information, you will find the Columns Actions section. This section informs you of the currently selected columns, for easy confirmation. Then, a Crop To button allows you to define a new DataFrame, named as indicated in the related text field, which will contain only the selected columns of the current DataFrame.

## Contributing

  The VoxR team feels that its solution is not ready for contributions yet. We can't wait to see what you have to offer though, and will open to contributions and explains how ASAP!

## License
[MIT](LICENSE)
